Tasks to answer in your own README.md that you submit on Canvas:

1.  See logger.log, why is it different from the log to console?
		logger.log is a log file. all of the logs will be printed to the fil as opposed to the console
1.  Where does this line come from? FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']
		This line comes from JUnit, it states that the method logResult is not disabled, and therefore should be run.
1.  What does Assertions.assertThrows do?
		It means that the function call in question is going to throw an exception and therefore should assert if it does not. 
1.  See TimerException and there are 3 questions
    1.  What is serialVersionUID and why do we need it? (please read on Internet)
    	This allows the object to be serialized and deserialized is compatible with the attempted type.
    2.  Why do we need to override constructors?
    	Java does not allow @overriding constructors. Otherwise you would be able to instantiate an object of a 
    	subclass without initializing the superclass object. In the exception class the constructors 
    	allow other classes to call `new TimerException()`
    3.  Why we did not override other Exception methods?	
    	Exception does not have any abstract methods. Also, exception has no methods that have functionality that 
    	we would like to alter in any way, however we could if we wanted to, as long as they are not final; 
1.  The Timer.java has a static block static {}, what does it do? (determine when called by debugger)
	static blocks are executed once when the obejct is initialized, usually used to initialize static members.
1.  What is README.md file format how is it related to bitbucket? (https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)
	.md is markdown. It is a markup format like LaTeX, but less powerful. Github and Bitbucket, as well as many coding forums support posts and documentation 
	in the format for ease of formatting
1.  Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)
	The test was failing because the current time was not set and when the fillay block was executed a nullpointer 
	exception was thrown instead of the TimingException.
1.  What is the actual issue here, what is the sequence of Exceptions and handlers (debug)
	The actual issue is that while the exception was being processed another exception was 
	thrown. This makes it difficult to debug.
1.  Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel) 
	ok
1.  Make a printScreen of your eclipse Maven test run, with console
	ok
1.  What category of Exceptions is TimerException and what is NullPointerException
	Null Pointer exception is an unckecked exception while TimerException, because it extends Exception is a checked exception.
1.  Push the updated/fixed source code to your own repository.